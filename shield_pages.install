<?php

/**
 * @file
 * Protected Pages install file.
 */

/**
 * Implements hook_schema().
 */
function shield_pages_schema() {
  $schema['shield_pages'] = array(
    'fields' => array(
      'spid' => array(
        'description' => 'The primary key always unique.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'password' => array(
        'description' => 'Keeps the passwords of the protected page.',
        'type' => 'blob',
        'not null' => TRUE,
      ),
      'path' => array(
        'type' => 'varchar',
        'description' => 'The path of the protected page.',
        'length' => '255',
        'not null' => TRUE,
      ),
    ),
    'indexes' => array(
      'path' => array('path'),
    ),
    'primary key' => array('spid'),
  );
  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function shield_pages_uninstall() {
  variable_del('shield_pages_user_global_password');
  variable_del('shield_pages_session_expire_time');
  variable_del('shield_pages_title');
  variable_del('shield_pages_description');
  variable_del('shield_pages_password_label');
  variable_del('shield_pages_submit_button_text');
  variable_del('shield_pages_global_password');
  variable_del('shield_pages_incorrect_password_msg');
}

/**
 * Implements hook_enable().
 */
function shield_pages_enable() {
  drupal_set_message(t('The Shield Pages module has been successfully enabled.
    Visit the <a href="@permissions">permissions</a>, and set the permissions.',
    array('@permissions' => url('admin/people/permissions', array('fragment' => 'module-shield_pages')))));
}
