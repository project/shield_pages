<?php

/**
 * @file
 * Provides page callbacks for configuration page.
 */

/**
 * Callback function for add shield page.
 */
function shield_pages_configure($form, &$form_state) {

  $form['rules_list'] = array(
    '#title' => t("Add Shield Page Relative path and passwords."),
    '#type' => 'fieldset',
    '#prefix' => '<div id="rules_list">',
    '#suffix' => '</div>',
    '#description' => t('Please enter the relative path and its corresponding
    password. When user opens this url, they will asked to enter password to
    view this page. For example, "node/5", "new-events" etc.'),
  );
  $form['rules_list']['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Relative Path'),
    '#description' => t('Specify pages by using their paths. Enter one path per line. The "/" character is a wildcard. Example paths are blog for the blog page and blog/ for every personal blog.'),
    '#required' => TRUE,
  );
  $form['rules_list']['password'] = array(
    '#type' => 'textarea',
    '#title' => t('Specify Passwords'),
    '#required' => TRUE,
    '#description' => t('Specify the passwords one per line.'),
  );
  $form['rules_list']['submit'] = array(
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'shield_pages_configure_submit_callback',
      'wrapper' => 'rules_list',
      'effect' => 'fade',
      'method' => 'replace',
      'progress' => array('type' => 'throbber', 'message' => t('Please wait. The data is being saved.')),
    ),
    '#value' => t('Save'),
  );

  $form['rules_list']['pages_table'] = array(
    '#markup' => shield_pages_get_pages_list(),
  );
  return $form;
}

/**
 * Callback to generate list of shield pages.
 */
function shield_pages_get_pages_list() {

  $header = array(
    array(
      'data' => t('#'),
      'width' => '10%',
    ),
    array(
      'data' => t('Relative Path'),
      'width' => '60%',
    ),
    array(
      'data' => t('Operations'),
      'colspan' => 3,
      'width' => '30%',
    ),
  );

  $rows = array();
  $query = db_select('shield_pages', 'pp')->extend('PagerDefault')->extend('TableSort');
  $query->fields('pp')
      ->limit(20)
      ->orderByHeader($header);

  $result = $query->execute();

  $rows = array();
  $count = 1;
  foreach ($result as $data) {
    $rows[] = array(
      'data' =>
      array(
        $count,
        $data->path,
        l(t('Edit'), 'admin/config/system/shield-pages/' . $data->spid . '/edit'),
        l(t('Delete'), 'admin/config/system/shield-pages/' . $data->spid . "/delete"),
      ),
    );
    $count++;
  }

  $output = theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array('width' => '500'),
    'sticky' => TRUE,
    'caption' => "",
    'colgroups' => array(),
    'empty' => t('No record found!'),
      )
  );

  $output .= theme('pager', array(
    'tags' => array(),
      )
  );

  return $output;
}

/**
 * Validate callback of add shield form page.
 *
 * @see shield_pages_configure()
 */
function shield_pages_configure_validate($form, &$form_state) {

  $path = check_plain($form_state['values']['path']);
  $realpath = rtrim($path, '/');
  $form_state['normal_path'] = drupal_get_normal_path($realpath);
  $path_alias = drupal_strtolower(drupal_get_path_alias($realpath));
  if (!drupal_valid_path($form_state['normal_path'])) {
    form_set_error('path', t('Please enter a correct path!'));
  }

  $spid = db_select('shield_pages')
      ->fields('shield_pages', array('spid'))
      ->condition(db_or()->condition('path', $form_state['normal_path'])->condition('path', $path_alias)->condition('path', $path))
      ->range(0, 1)
      ->execute()
      ->fetchField();
  if ($spid) {
    form_set_error('path', t('Duplicate path entry is not allowed. There is already a path or its alias exists.'));
  }
}

/**
 * Ajax submit callback for add shield page form.
 */
function shield_pages_configure_submit_callback($form, &$form_state) {

  $errors = form_get_errors();

  if (count($errors) < 1) {

    $shield_page = new stdclass();
    $shield_page->password = $form_state['values']['password'];
    $shield_page->path = $form_state['values']['path'];
    drupal_write_record('shield_pages', $shield_page);
    drupal_set_message(t('The Shield page settings has been successfully saved.'));
    $form['rules_list']['pages_table']['#markup'] = shield_pages_get_pages_list();
    $form['rules_list']['path']['#value'] = '';
  }

  return $form['rules_list'];
}

/**
 * Callback function for edit shield page form.
 */
function shield_pages_edit($form, &$form_state, $spid) {

  $shield_page = shield_pages_shield_page_load($spid);

  if (!isset($shield_page->path)) {
    drupal_access_denied();
    drupal_exit();
  }
  $form['rules_list'] = array(
    '#title' => t("Edit Shield Page Relative path and password."),
    '#type' => 'fieldset',
    '#prefix' => '<div id="rules_list">',
    '#suffix' => '</div>',
    '#description' => t('Please enter the relative path and its corresponding
    password. When user opens this url, they will asked to enter password to
    view this page. For example, "node/5", "new-events" etc.'),
  );
  $form['rules_list']['path'] = array(
    '#type' => 'textfield',
    '#title' => t('Relative Path'),
    '#description' => t('Specify pages by using their paths. Enter one path per
      line. The "/" character is a wildcard. Example paths are blog for the blog
      page and node/ for every personal blog.'),
    '#required' => TRUE,
    '#default_value' => $shield_page->path,
  );
  $form['rules_list']['password'] = array(
    '#type' => 'textarea',
    '#title' => t('Specify Passwords'),
    '#required' => TRUE,
    '#description' => t('Specify the passwords one per line.'),
    '#default_value' => $shield_page->password,
  );
  $form['rules_list']['spid'] = array(
    '#type' => 'hidden',
    '#value' => $spid,
  );
  $form['rules_list']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate callback of edit shield form page.
 *
 * @see shield_pages_edit()
 */
function shield_pages_edit_validate($form, &$form_state) {
  $path = $form_state['values']['path'];
  $realpath = rtrim($path, '/');
  $form_state['normal_path'] = drupal_get_normal_path($realpath);
  if (!drupal_valid_path($form_state['normal_path'])) {
    form_set_error('path', t('Please enter a correct path!'));
  }
  $spid = db_select('shield_pages')
      ->fields('shield_pages', array('spid'))
      ->condition(db_or()->condition('path', $form_state['normal_path'])->condition('path', $form_state['values']['path']))
      ->condition('spid', $form_state['values']['spid'], '<>')
      ->range(0, 1)
      ->execute()
      ->fetchField();
  if ($spid) {
    form_set_error('path', t('Duplicate path entry is not allowed. There is already a path or its alias exists.'));
  }
}

/**
 * Submit Callback of edit shield form page.
 *
 * @see shield_pages_edit()
 */
function shield_pages_edit_submit($form, &$form_state) {

  $shield_page = new stdclass();
  $shield_page->password = $form_state['values']['password'];
  $shield_page->path = $form_state['values']['path'];
  $shield_page->spid = $form_state['values']['spid'];
  if ($shield_page->spid) {
    drupal_write_record('shield_pages', $shield_page, 'spid');
  }
  else {
    drupal_write_record('shield_pages', $shield_page);
  }
  $form_state['redirect'] = 'admin/config/system/shield-pages';
}

/**
 * Callback function for delete shield form page.
 */
function shield_pages_delete_confirm($form, &$form_state, $spid) {

  $path = db_select('shield_pages')
      ->fields('shield_pages', array('path'))
      ->condition('spid', $spid)
      ->range(0, 1)
      ->execute()
      ->fetchField();

  $form['spid'] = array('#type' => 'hidden', '#value' => $spid);
  return confirm_form($form, t('Are you sure you want to delete <b>"%path"</b> from shield pages list?', array('%path' => $path)), 'admin/config/system/shield-pages', t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

/**
 * Submit Callback of delete shield form page.
 *
 * @see shield_pages_delete_confirm()
 */
function shield_pages_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $spid = $form_state['values']['spid'];

    db_delete('shield_pages')
        ->condition('spid', $spid)
        ->execute();

    drupal_set_message(t('The path has been successfully deleted from shield pages.'));
    $form_state['redirect'] = 'admin/config/system/shield-pages';
  }
}

/**
 * Callback function for shield pages settings.
 */
function shield_pages_settings() {
  $form['shield_pages_password_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Shield Pages Password Settings'),
    '#description' => t('Configure password related settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $global_password_help_text = array();

  $global_password_help_text[] = t('Allow per page password = Only per page password will be accepted. Global password will not be accepted.');
  $global_password_help_text[] = t('Allow per page password or Global password = Per page  password and global password both will be accepted.');
  $global_password_help_text[] = t('Allow Only Global = Only global password will be accepted for each protected page.');

  $form['shield_pages_password_fieldset']['shield_pages_user_global_password'] = array(
    '#type' => 'select',
    '#title' => t('Global Password Setting'),
    '#default_value' => variable_get('shield_pages_user_global_password', 'per_page_password'),
    '#options' => array(
      'per_page_password' => t('Allow per page password'),
      'per_page_or_global' => t('Allow per page password or Global password'),
      'only_global' => t('Allow Only Global'),
    ),
    '#description' => t('Please select the appropriate option for shield pages handling.') .
    '<br />' . theme('item_list', array('items' => $global_password_help_text)),
  );

  $form['shield_pages_password_fieldset']['shield_pages_global_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Global Password'),
    '#description' => t('The default password for all shield pages. This
      password is necessary if you select the previous checkbox "Allow per page
      password or Global password" or "Allow Only Global" options above.'),
    '#default_value' => variable_get('shield_pages_global_password', 0),
  );
  $form['shield_pages_password_fieldset']['shield_pages_session_expire_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Session Expire Time'),
    '#description' => t('When user enters password a session is created.
      The node will be accessible until session expire. Once session expires,
      user will need to enter password again. The default session expire time
      is 0 (unlimited).'),
    '#default_value' => variable_get('shield_pages_session_expire_time', 0),
    '#required' => TRUE,
    '#size' => 10,
    '#element_validate' => array('shield_pages_validate_integer_positive'),
    '#field_suffix' => t('in minutes'),
  );

  $form['shield_pages_other_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Shield Pages Other Settings'),
    '#description' => t('Configure other settings.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['shield_pages_other_fieldset']['shield_pages_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Password page title'),
    '#default_value' => variable_get('shield_pages_title', t('Shield Page -- Enter password')),
    '#description' => t('Enter the title of the Shield page.'),
  );
  $form['shield_pages_other_fieldset']['shield_pages_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Password page description (inside the field set)'),
    '#default_value' => variable_get('shield_pages_description', t('The page you are trying to view is password protected. Please enter the password below to proceed.')),
    '#description' => t('Enter specific description for the protected page. This description is displayed inside the fieldset. HTML is accepted.'),
  );
  $form['shield_pages_other_fieldset']['shield_pages_password_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Password field label'),
    '#default_value' => variable_get('shield_pages_password_label', t('Enter Password')),
    '#description' => t('Enter the text for the password field label.'),
  );
  $form['shield_pages_other_fieldset']['shield_pages_submit_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Submit Button Text'),
    '#default_value' => variable_get('shield_pages_submit_button_text', t('Authenticate')),
    '#description' => t('Enter the text for the submit button of enter password form.'),
  );
  $form['shield_pages_other_fieldset']['shield_pages_incorrect_password_msg'] = array(
    '#type' => 'textarea',
    '#title' => t('Incorrect Password Error Text'),
    '#default_value' => variable_get('shield_pages_incorrect_password_msg', t('Incorrect password!')),
    '#description' => t('This error text will appear if someone enters wrong password in "Enter password screen".'),
  );
  return system_settings_form($form);
}

/**
 * Callback function to validate session expire time value.
 */
function shield_pages_validate_integer_positive($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name must be a positive integer.', array('%name' => $element['#title'])));
  }
}
