CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module allows you to protect any page of your website by secure keys.
You can enter urls of pages to protect and set multiple keys per page.
Admin (uid = 1) or user with bypass protection permission can view page.

REQUIREMENTS
------------
 * None

RECOMMENDED MODULES
-------------------
 * Markdown filter (https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.txt help will be rendered
   with markdown.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure the settings provided by Shield Pages Module in
   Administration » Configuration » System:
 * Restrict the pages with in the keys in
   Administration » Configuration » System:
 * Configure the permissions provided by Shield Pages Module in
   Administration » People » Permissions:

MAINTAINERS
-----------
Current maintainers:
 * Naveen Valecha (naveenvalecha) - https://drupal.org/u/naveenvalecha
