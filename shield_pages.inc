<?php

/**
 * @file
 * Provides page callbacks for enter password page.
 */

/**
 * Callback function for enter password page.
 */
function shield_pages_enter_password($form, &$form_state) {
  if (!isset($_GET['destination']) || empty($_GET['shield_page']) || !is_numeric($_GET['shield_page'])) {
    watchdog('shield_pages', 'Illegal call to /shield-page', array(), WATCHDOG_WARNING);
    drupal_access_denied();
    drupal_exit();
  }
  $spid = db_select('shield_pages')
      ->fields('shield_pages', array('spid'))
      ->condition('spid', $_GET['shield_page'])
      ->range(0, 1)
      ->execute()
      ->fetchField();
  if (!$spid) {
    watchdog('shield_pages', 'Invalid spid (@spid) used with /shield-page', array('@spid' => $_GET['shield_page']), WATCHDOG_WARNING);
    drupal_access_denied();
    exit();
  }
  $form['shield_page_enter_password'] = array(
    '#type' => 'fieldset',
    '#description' => variable_get('shield_pages_description', t('The page you are trying to view is password protected. Please enter the password below to proceed.')),
    '#collapsible' => FALSE,
  );

  $form['shield_page_enter_password']['password'] = array(
    '#type' => 'password',
    '#title' => variable_get('shield_pages_password_label', t('Enter Password')),
    '#size' => 20,
    '#required' => TRUE,
  );

  $form['shield_page_spid'] = array(
    '#type' => 'hidden',
    '#value' => $_GET['shield_page'],
  );

  $form['shield_page_enter_password']['submit'] = array(
    '#type' => 'submit',
    '#value' => variable_get('shield_pages_submit_button_text', t('Authenticate')),
  );

  return $form;
}

/**
 * Validate handler for enter password page.
 *
 * @see shield_pages_enter_password()
 */
function shield_pages_enter_password_validate($form, &$form_state) {
  $password = $form_state['values']['password'];
  $global_password_setting = variable_get('shield_pages_user_global_password', 'per_page_password');
  if ($global_password_setting == 'per_page_password') {
    $shield_page = shield_pages_shield_page_load($form_state['values']['shield_page_spid']);
    $passwords = explode(PHP_EOL, $shield_page->password);
    if ($shield_page->spid && !in_array($password, $passwords)) {
      form_set_error('password', variable_get('shield_pages_incorrect_password_msg', t('Incorrect password!')));
    }
  }
  elseif ($global_password_setting == 'per_page_or_global') {
    $shield_page = shield_pages_shield_page_load($form_state['values']['shield_page_spid']);
    $passwords = explode(PHP_EOL, $shield_page->password);
    $global_password = variable_get('shield_pages_global_password', '');
    if (($shield_page->spid && !in_array($password, $passwords)) || $global_password != $password) {
      form_set_error('password', variable_get('shield_pages_incorrect_password_msg', t('Incorrect password!')));
    }
  }
  else {
    $global_password = variable_get('shield_pages_global_password', '');
    if ($global_password != $password) {
      form_set_error('password', variable_get('shield_pages_incorrect_password_msg', t('Incorrect password!')));
    }
  }
}

/**
 * Submit handler for enter password page.
 *
 * @see shield_pages_enter_password()
 */
function shield_pages_enter_password_submit($form, &$form_state) {
  $_SESSION['_shield_page']['passwords'][$form_state['values']['shield_page_spid']]['request_time'] = REQUEST_TIME;
  $session_expire_time = variable_get('shield_pages_session_expire_time', 0);
  if ($session_expire_time) {
    $_SESSION['_shield_page']['passwords'][$form_state['values']['shield_page_spid']]['expire_time'] = strtotime("+{$session_expire_time} minutes");
  }
}
